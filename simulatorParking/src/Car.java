public class Car {
    private int countMove;
    private int uniNumber;

    public Car(int countMove, int uniNumber) {
        this.countMove = countMove;
        this.uniNumber = uniNumber;
    }

    public Car() {
    }

    public void setCountMove(int countMove) {
        this.countMove = countMove;
    }

    public int getCountMove() {
        return countMove;
    }

    public int getUniNumber() {
        return uniNumber;
    }
}
