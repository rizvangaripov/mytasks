import java.util.ArrayList;
import java.util.Scanner;

public class Parking {
    Scanner scanner = new Scanner(System.in);
    private int placesCar = scanner.nextInt();
    private int placesTruck = scanner.nextInt();
    private ArrayList<Car> occupiedPlacesCar = new ArrayList<>();
    private ArrayList<Truck> occupiedPlacesTruck = new ArrayList<>();
    Car car = new Car();
    Truck truck = new Truck();

    public Parking() {
        for (int i = 0; i < placesCar; i++) {
            occupiedPlacesCar.add(i, null);
        }
        for (int i = 0; i < placesTruck; i++) {
            occupiedPlacesTruck.add(i, null);
        }
    }

    public Car getOccupiedPlacesCar(int i) {
        return occupiedPlacesCar.get(i);
    }

    public void setOccupiedPlacesCar(Car car, int i) {
        this.occupiedPlacesCar.set(i, car);
    }

    public int getPlacesCar() {
        return placesCar;
    }

    public Truck getOccupiedPlacesTruck(int i) {
        return occupiedPlacesTruck.get(i);
    }

    public void setOccupiedPlacesTruck(Truck truck, int i) {
        this.occupiedPlacesTruck.set(i, truck);
    }

    public int getPlacesTruck() {
        return placesTruck;
    }
}
