import java.util.Random;
import java.util.Scanner;

public class ParkingLotManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Enter count of places in parking for cars, then for trucks");
        Parking parking = new Parking();
        int uniNumber = 1000;
        int minCar = 10;
        int minTruck = 10;
        int countOccupiedPlacesCars = 0;
        int countOccupiedPlacesTrucks = 0;
        boolean isFreeCar = false;
        boolean isFreeTruck = false;
        for (; ;) {
            if (minCar == 0) {
                minCar = 10;
            }
            if (minTruck == 0) {
                minTruck = 10;
            }
            int countArrivedCars = random.nextInt((parking.getPlacesCar() + parking.getPlacesTruck()) / 3);
            for (int i = 0; i < countArrivedCars; i++) {
                Car car = new Car(random.nextInt(11), uniNumber);
                for (int j = 0; j < parking.getPlacesCar(); j++) {
                    isFreeCar = false;
                    if (parking.getOccupiedPlacesCar(j) == null) {
                        parking.setOccupiedPlacesCar(car, j);
                        countOccupiedPlacesCars++;
                        isFreeCar = true;
                        uniNumber = incUniNumber(uniNumber);
                        minCar = searchMin(j, minCar, parking, "car");
                        break;
                    }
                }
            }
            int countArrivedTrucks = random.nextInt((parking.getPlacesTruck() + parking.getPlacesCar()) / 3);
            for (int i = 0; i < countArrivedTrucks; i++) {
                Truck truck = new Truck(random.nextInt(11), uniNumber);
                for (int j = 0; j < parking.getPlacesTruck(); j++) {
                    isFreeTruck = false;
                    if (parking.getOccupiedPlacesTruck(j) == null) {
                        parking.setOccupiedPlacesTruck(truck, j);
                        countOccupiedPlacesTrucks++;
                        isFreeTruck = true;
                        uniNumber = incUniNumber(uniNumber);
                        minTruck = searchMin(j, minTruck, parking, "truck");
                        break;
                    }
                }
                    if (!isFreeTruck) {
                        for (int j = 0; j < parking.getPlacesCar() - 1; j++) {
                            if (parking.getOccupiedPlacesCar(j) == null &
                                    parking.getOccupiedPlacesCar(j + 1) == null) {
                                parking.setOccupiedPlacesCar(truck, j);
                                parking.setOccupiedPlacesCar(truck, j + 1);
                                countOccupiedPlacesCars += 2;
                                isFreeTruck = true;
                                uniNumber = incUniNumber(uniNumber);
                                minCar = searchMin(j, minCar, parking, "car");
                                break;
                            }
                        }
                    }
            }
            outNoPlaces(isFreeCar, "car", minCar);
            outNoPlaces(isFreeTruck, "truck", minTruck);
            text();
            int action = scanner.nextInt();
            while (action != 1) {
                if (action == 2) {
                    System.out.println("Parking places for cars");
                    System.out.println("Count moves  uniNumber");
                    for (int i = 0; i < parking.getPlacesCar(); i++) {
                        if (parking.getOccupiedPlacesCar(i) == null) {
                            System.out.println(0);
                        } else {
                            System.out.println(parking.getOccupiedPlacesCar(i).getCountMove() + "             " +
                                    parking.getOccupiedPlacesCar(i).getUniNumber());
                        }
                    }
                    System.out.println("Parking places for trucks");
                    System.out.println("Count moves  uniNumber");
                    for (int i = 0; i < parking.getPlacesTruck(); i++) {
                        if (parking.getOccupiedPlacesTruck(i) == null) {
                            System.out.println(0);
                        } else {
                            System.out.println(parking.getOccupiedPlacesTruck(i).getCountMove() + "             " +
                                    parking.getOccupiedPlacesTruck(i).getUniNumber());
                        }
                    }
                } else if (action == 3) {
                    System.out.println("Count occupied places in car parking");
                    System.out.println(countOccupiedPlacesCars);
                    System.out.println("Count occupied places in truck parking");
                    System.out.println(countOccupiedPlacesTrucks);
                    } else if (action == 4) {
                        System.out.println("Count free places in car parking");
                        System.out.println(parking.getPlacesCar() - countOccupiedPlacesCars);
                        System.out.println("Count free places in truck parking");
                        System.out.println(parking.getPlacesTruck() - countOccupiedPlacesTrucks);
                    } else if (action == 5) {
                    System.out.println("Min for cars:");
                        System.out.println(minCar);
                    System.out.println("Min for trucks:");
                    System.out.println(minTruck);
                    } else if (action == 6) {
                        for (int i = 0; i < parking.getPlacesCar(); i++) {
                                parking.setOccupiedPlacesCar(null, i);
                        }
                    for (int i = 0; i < parking.getPlacesTruck(); i++) {
                        parking.setOccupiedPlacesTruck(null, i);
                    }
                        minCar = 0;
                        minTruck = 0;
                        countOccupiedPlacesCars = 0;
                        countOccupiedPlacesTrucks = 0;
                        System.out.println("Parking places for cars");
                        System.out.println("Count moves  uniNumber");
                        for (int i = 0; i < parking.getPlacesCar(); i++) {
                                System.out.println(0);
                        }
                        System.out.println("Parking places for trucks");
                        System.out.println("Count moves  uniNumber");
                        for (int i = 0; i < parking.getPlacesTruck(); i++) {
                        System.out.println(0);
                        }
                    }
                text();
                    action = scanner.nextInt();
                }
            if (action == 1) {
                for (int i = 0; i < parking.getPlacesCar(); i++) {
                    if (parking.getOccupiedPlacesCar(i) != null) {
                        parking.getOccupiedPlacesCar(i).setCountMove(parking.getOccupiedPlacesCar(i).getCountMove()
                                - 1);
                        if (parking.getOccupiedPlacesCar(i).getCountMove() == 0 |
                                parking.getOccupiedPlacesCar(i).getCountMove() == -1) {
                            parking.setOccupiedPlacesCar(null, i);
                            countOccupiedPlacesCars--;
                            minCar = 10;
                            for (int j = 0; j < parking.getPlacesCar(); j++) {
                                    minCar = searchMin(j, minCar, parking, "car");
                            }
                        } else {
                            minCar = searchMin(i, minCar, parking, "car");
                        }
                    }
                }
                for (int i = 0; i < parking.getPlacesTruck(); i++) {
                    if (parking.getOccupiedPlacesTruck(i) != null) {
                        parking.getOccupiedPlacesTruck(i).setCountMove(parking.getOccupiedPlacesTruck(i).getCountMove()
                                - 1);
                        if (parking.getOccupiedPlacesTruck(i).getCountMove() == 0 |
                                parking.getOccupiedPlacesTruck(i).getCountMove() == -1) {
                            parking.setOccupiedPlacesTruck(null, i);
                            countOccupiedPlacesTrucks--;
                            minTruck = 10;
                            for (int j = 0; j < parking.getPlacesTruck(); j++) {
                                minTruck = searchMin(j, minTruck, parking, "truck");
                            }
                        } else {
                            minTruck = searchMin(i, minTruck, parking, "truck");
                        }
                    }
                }
            }
        }
    }

   public static void text() {
        System.out.println("Enter 1 - next move");
        System.out.println("Enter 2 - view information about parking");
        System.out.println("Enter 3 - view count of occupied places");
        System.out.println("Enter 4 - view count of free places");
        System.out.println("Enter 5 - find out when nearest place becomes available");
        System.out.println("Enter 6 - clear parking");
    }

   public static int searchMin(int i, int min, Parking parking, String carOrTruck) {
        if (carOrTruck.equals("car")) {
            if (parking.getOccupiedPlacesCar(i) != null && min > parking.getOccupiedPlacesCar(i).getCountMove()) {
                min = parking.getOccupiedPlacesCar(i).getCountMove();
            }
        } else {
            if (parking.getOccupiedPlacesTruck(i) != null && min > parking.getOccupiedPlacesTruck(i).getCountMove()) {
                min = parking.getOccupiedPlacesTruck(i).getCountMove();
            }
        }
        return min;
    }

    public static int incUniNumber(int uniNumber) {
        uniNumber++;
        if (uniNumber > 9999) {
            uniNumber = 1000;
        }
        return uniNumber;
    }

    public static void outNoPlaces(boolean isFree, String carOrTruck, int min) {
        if (!isFree) {
            if (carOrTruck.equals("car")) {
                System.out.println("No free places in car parking. Left " + min + " move(s) until place becomes" +
                        " available");
            } else {
                System.out.println("No free places in truck parking. Left " + min + " move(s) until place becomes" +
                        " available");
            }
        }
    }
}

